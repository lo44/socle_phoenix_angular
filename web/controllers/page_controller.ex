defmodule SoclePhoenixAngular.PageController do
  use SoclePhoenixAngular.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
