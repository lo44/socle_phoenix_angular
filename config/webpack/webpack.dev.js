var webpackMerge = require('webpack-merge');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');

module.exports = webpackMerge(commonConfig, {
    devtool: 'cheap-module-eval-source-map',

    watch: true,
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000
    },

    output: {
        path: helpers.root('priv', 'static', 'js'),
        filename: '[name].js'
    },

    plugins: [
        new ExtractTextPlugin('[name].css')
    ]

});