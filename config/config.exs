# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :socle_phoenix_angular,
  ecto_repos: [SoclePhoenixAngular.Repo]

# Configures the endpoint
config :socle_phoenix_angular, SoclePhoenixAngular.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "L/Epmpt6mxGvXiSkrDQdOacxOBkZ61Ah0an9kiOA3iy80uIgMYNcOJZYJwOr6nDW",
  render_errors: [view: SoclePhoenixAngular.ErrorView, accepts: ~w(html json)],
  pubsub: [name: SoclePhoenixAngular.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
